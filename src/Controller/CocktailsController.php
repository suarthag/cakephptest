<?php
namespace App\Controller;
use App\Controller\AppController;
class CocktailsController extends AppController
{
    public $paginate = [
        'page' => 1,
        'limit' => 10,
        'maxLimit' => 100,
        'fields' => [
            'id', 'name', 'description','created'
        ],
        'sortWhitelist' => [
            'id', 'name', 'description','created'
        ]
    ];
}